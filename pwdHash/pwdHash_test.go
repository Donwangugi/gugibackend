package pwdHash

import (
	"testing"
)

func Test_PwdHash_Success(t *testing.T) {
	password := "password"
	passBytes := []byte(password)
	hashedBytes, err := HashPasswordString(password)
	if err != nil {
		t.Errorf("Error while trying to hash password: %q", err.Error())
	}

	err = CompareHashAndPass(hashedBytes, passBytes)
	if err != nil {
		t.Errorf("Error, want %q, err message: %q", hashedBytes, err.Error())
	}
}

func Test_PwdHash__Salt_Success(t *testing.T) {
	password := "password"
	passBytes := []byte(password)
	salt, err := GenerateSalt()
	if err != nil {
		t.Errorf("Error while generating salt: %q", err.Error())
	}

	hashedBytes, err := HashPassword(append(salt, passBytes...))
	if err != nil {
		t.Errorf("Error while trying to hash password: %q", err.Error())
	}

	err = CompareHashAndPass(hashedBytes, append(salt, passBytes...))
	if err != nil {
		t.Errorf("Error, want %q, err message: %q", hashedBytes, err.Error())
	}
}

func Test_PwdHash__SaltConvertBetweenString_Success(t *testing.T) {
	password := "password"
	passBytes := []byte(password)
	salt, err := GenerateSalt()
	if err != nil {
		t.Errorf("Error while generating salt: %q", err.Error())
	}

	hashedBytes, err := HashPassword(append(salt, passBytes...))
	if err != nil {
		t.Errorf("Error while trying to hash password: %q", err.Error())
	}

	saltString := string(salt[:])
	saltStr := []byte(saltString)

	err = CompareHashAndPass(hashedBytes, append(saltStr, passBytes...))
	if err != nil {
		t.Errorf("Error, want %q, err message: %q", hashedBytes, err.Error())
	}
}

func Test_PwdHash_Fail(t *testing.T) {
	password := "password"
	hashedBytes, err := HashPasswordString(password)
	if err != nil {
		t.Errorf("Error while trying to hash password: %q", err.Error())
	}

	wrongPass := []byte("Wrong Pass!")
	err = CompareHashAndPass(hashedBytes, wrongPass)
	if err == nil {
		t.Error("Error passwords should not match")
	}
}

func Test_PwdHash_WithoutSalt_Fail(t *testing.T) {
	password := "password"
	passBytes := []byte(password)
	salt, err := GenerateSalt()
	if err != nil {
		t.Errorf("Error while generating salt: %q", err.Error())
	}

	hashedBytes, err := HashPassword(append(salt, passBytes...))
	if err != nil {
		t.Errorf("Error while trying to hash password: %q", err.Error())
	}

	err = CompareHashAndPass(hashedBytes, passBytes)
	if err == nil {
		t.Error("Error passwords should not match!")
	}
}
