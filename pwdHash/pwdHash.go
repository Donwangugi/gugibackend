package pwdHash

import (
	"crypto/rand"
	"golang.org/x/crypto/bcrypt"
)

const Cost int = 10
const Salt_Size_Bytes int = 8

func HashPasswordString(password string) ([]byte, error) {
	passBytes := []byte(password)
	return HashPassword(passBytes)
}

func HashPassword(password []byte) ([]byte, error) {
	return bcrypt.GenerateFromPassword(password, Cost)
}

func GenerateSalt() ([]byte, error) {
	b := make([]byte, Salt_Size_Bytes)
	_, err := rand.Read(b)
	return b, err
}

func CompareHashAndPass(hash, password []byte) error {
	return bcrypt.CompareHashAndPassword(hash, password)
}
