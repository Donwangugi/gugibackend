package main

type jsonErr struct {
	Text   string `json:"Text"`
	Logged bool   `json:"Logged"`
}

type jsonSuccess_UserPut struct {
	Text   string `json:"Text"`
	Token  string `json:"Token"`
	Logged bool   `json:"Logged"`
}
