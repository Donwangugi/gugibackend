package main

import (
	"bitbucket.org/gugibackend/models"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Welcome!\n")
}

func (env *Env) UserGet(c *gin.Context) {
	usr, err := env.db.GetUser(c.Param("name"))
	if err == nil {
		if usr == nil {
			c.JSON(http.StatusNotFound, jsonErr{Text: "Not Found", Logged: false})
		} else {
			c.JSON(http.StatusOK, *usr)
		}
	} else {
		c.JSON(http.StatusBadRequest, jsonErr{Text: "Bad Request", Logged: false})
	}
}

func (env *Env) UserPut(c *gin.Context) {
	result, usrSaved := c.Get(User_Struct)
	resultUser := result.(models.User)

	if !usrSaved {
		c.JSON(http.StatusBadRequest, jsonErr{Text: models.Unable_Retrieve_User_Context, Logged: false})
		return
	}

	if c.Param("name") != resultUser.Username {
		c.JSON(http.StatusBadRequest, jsonErr{Text: models.Mismatching_Username_Data, Logged: false})
		return
	}

	err := env.db.CreateUser(&resultUser)
	if err != nil {
		c.JSON(http.StatusOK, jsonErr{Text: err.Error(), Logged: false}) //TODO: Figure out how to convert struct to map
		return
	}

	c.Set(User_Struct, &resultUser)
}

/*
func (env *Env) UserDelete(c *gin.Context) {
	var json models.User
	if c.BindJSON((&json) == nil {
		err := env.db.DeleteUser(&json)
		if err != nil {
			c.JSON(code, obj)
		}
	}
} */

/*
Test with this curl command:
curl -H "Content-Type: application/json" -d '{"name":"New Todo"}' http://localhost:8080/todos
*/
/*
func UserCreate(w http.ResponseWriter, r *http.Request) {
	var todo Todo
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		panic(err)
	}
	if err := r.Body.Close(); err != nil {
		panic(err)
	}
	if err := json.Unmarshal(body, &todo); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422) // unprocessable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			panic(err)
		}
	}

	t := RepoCreateTodo(todo)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	if err := json.NewEncoder(w).Encode(t); err != nil {
		panic(err)
	}
}
*/

/*
func TodoIndex(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(todos); err != nil {
		panic(err)
	}
}
*/
