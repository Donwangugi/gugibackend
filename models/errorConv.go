package models

import (
	"errors"
	"strings"
)

const Phone_number_in_use string = "Phone number already in use. Please register"
const Username_taken string = "Username taken"
const Unknown_Error string = "Unkown error"
const Success string = "Success"
const Mismatching_Username_Data string = "Resource username does not match json"
const Unable_Retrieve_User_Context string = "Unable to retrieve user from context."

const unique_violation string = "unique_violation"

func convertPqErr(errCode string, errStr string) error {
	switch {
	case errCode == unique_violation:
		if strings.Contains(errStr, "users_username_key") {
			return errors.New(Username_taken)
		} else if strings.Contains(errStr, "users_phone_key") {
			return errors.New(Phone_number_in_use)
		}
	default:
		return errors.New(Unknown_Error)
	}
	return errors.New(Unknown_Error)
}
