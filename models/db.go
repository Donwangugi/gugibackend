package models

import (
	"database/sql"
)

type Datastore interface {
	GetUser(username string) (*User, error)
	CreateUser(usr *User) error
	DeleteUser(usr *User) error
	LookupPass(username string) ([]byte, error)
}

type DB struct {
	*sql.DB
}

type Tx struct {
	*sql.Tx
}

func Open(dataSourceName string) (*DB, error) {
	db, err := sql.Open("postgres", dataSourceName)
	if err != nil {
		return nil, err
	}
	if err = db.Ping(); err != nil {
		return nil, err
	}
	return &DB{db}, nil
}

func (db *DB) Begin() (*Tx, error) {
	tx, err := db.DB.Begin()
	if err != nil {
		return nil, err
	}
	return &Tx{tx}, nil
}
