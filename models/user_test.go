package models

import (
	"os"
	"testing"
)

func Test_Db_Access(t *testing.T) {
	//db, err := Open(os.Getenv("TEST_DATABASE_URL" + "?sslmode=disable"))
	_, err := Open(os.Getenv("TEST_DATABASE_URL") + "?sslmode=disable")
	if err != nil {
		t.Error(err)
	}
}

func Test_GetUser_NoUser(t *testing.T) {
	db, err := Open(os.Getenv("TEST_DATABASE_URL") + "?sslmode=disable")
	if err != nil {
		t.Error(err)
	}

	in := "bro"

	usr, err := db.GetUser(in)
	if usr != nil && err == nil {
		t.Errorf("GetUser(%q) => %q, want nil", in, usr.Print())
	}
}

func Test_GetUser_UserExists(t *testing.T) {
	db, err := Open(os.Getenv("TEST_DATABASE_URL") + "?sslmode=disable")
	if err != nil {
		t.Error(err)
	}

	desiredUser := User{Username: "gugi", Phone: "1234567890"}
	in := "gugi"
	usr, err := db.GetUser(in)
	if err != nil {
		t.Errorf("GetUser(%q) => Unexpected Error: %q", in, err.Error())
	}
	if usr == nil {
		t.Errorf("GetUser(%q) => nil, want %q", in, desiredUser.Print())
	}

	if usr.Username != desiredUser.Username || usr.Phone != desiredUser.Phone {
		t.Errorf("GetUser(%q) => %q, want %q", in, usr.Print(), desiredUser.Print())
	}
}

func Test_GetUser_UserExistsUpperCase(t *testing.T) {
	db, err := Open(os.Getenv("TEST_DATABASE_URL") + "?sslmode=disable")
	if err != nil {
		t.Error(err)
	}

	desiredUser := User{Username: "gugi", Phone: "1234567890"}
	in := "GUGI"
	usr, err := db.GetUser(in)
	if err != nil {
		t.Errorf("GetUser(%q) => Unexpected Error: %q", in, err.Error())
	}
	if usr == nil {
		t.Errorf("GetUser(%q) => nil, want %q", in, desiredUser.Print())
	} else if usr.Username != desiredUser.Username || usr.Phone != desiredUser.Phone {
		t.Errorf("GetUser(%q) => %q, want %q", in, usr.Print(), desiredUser.Print())
	}
}

func Test_CreateUser_FailureUsernameExists(t *testing.T) {
	db, err := Open(os.Getenv("TEST_DATABASE_URL") + "?sslmode=disable")
	if err != nil {
		t.Error(err)
	}

	usr := User{Username: "gugi", Phone: "1234567890"}
	err = db.CreateUser(&usr)
	if err.Error() != Username_taken {
		t.Errorf("CreateUser(%q) => %q, want %q", usr.Username, err.Error(), Username_taken)
	}
}

func Test_CreateUser_FailurePhoneNumberExists(t *testing.T) {
	db, err := Open(os.Getenv("TEST_DATABASE_URL") + "?sslmode=disable")
	if err != nil {
		t.Error(err)
	}

	usr := User{Username: "newUser", Phone: "1234567890"}
	err = db.CreateUser(&usr)
	if err.Error() != Phone_number_in_use {
		t.Errorf("CreateUser(%q) => %q, want %q", usr.Username, err.Error(), Phone_number_in_use)
	}
}

func Test_GetPassword_Success(t *testing.T) {
	db, err := Open(os.Getenv("TEST_DATABASE_URL") + "?sslmode=disable")
	if err != nil {
		t.Error(err)
	}

	usr := User{Username: "gugi", Phone: "1234567890"}
	pass, err := db.LookupPass(usr.Username)
	if err != nil {
		t.Errorf("LookupPass(%q) => %q", usr.Username, err.Error())
	}

	if pass == nil {
		t.Errorf("LookupPass(%q) => nil", usr.Username)
	}
}

func Test_GetPassword_FailNoUser(t *testing.T) {
	db, err := Open(os.Getenv("TEST_DATABASE_URL") + "?sslmode=disable")
	if err != nil {
		t.Error(err)
	}

	usr := User{Username: "no_such_user", Phone: "1234567890"}
	pass, err := db.LookupPass(usr.Username)
	if err != nil {
		t.Errorf("LookupPass(%q) => %q", usr.Username, err.Error())
	}

	if pass != nil {
		t.Errorf("LookupPass(%q) => %q", usr.Username, pass)
	}
}

/* Currently commented out, would need to create and then delete a user
func Test_DeleteUser_Success(t *testing.T) {
	db, err := Open(os.Getenv("TEST_DATABASE_URL") + "?sslmode=disable")
	if err != nil {
		t.Error(err)
	}

	usr := User{Id: 1, Username: "newUser", Created: time.Now(), Phone: "1234567890", Password: "blargh"}
	err = db.DeleteUser(&usr)
	if err != nil {
		t.Errorf("DeleteUser(%q) => %q, want nil", usr.Username, err.Error(), nil)
	}
}*/
