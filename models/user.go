package models

import (
	"bitbucket.org/gugibackend/Godeps/_workspace/src/github.com/lib/pq"
	"database/sql"
	"log"
	"strconv"
	"strings"
	"time"
)

type User struct {
	Id       int       `json:"id"`
	Username string    `json:"username"`
	Created  time.Time `json:"created"`
	Phone    string    `json:"phone"`
	Password string    `json:"password"`
	Salt     string    `json:"salt"`
}

func (usr *User) Print() string {
	if usr == nil {
		return "nil"
	}
	s := []string{"id: " + strconv.Itoa(usr.Id), "username: " + usr.Username, "created: " + usr.Created.String(), "phone: " + usr.Phone}
	return strings.Join(s, ", ")
}

type Users []User

func (db *DB) CreateUser(usr *User) error {
	tx, _ := db.Begin()
	err := tx.CreateUser(usr)
	if err != nil {
		pqErr := err.(*pq.Error)
		log.Println(pqErr.Code)
		log.Println(pqErr)
		return convertPqErr(pqErr.Code.Name(), pqErr.Error())
	}
	err = tx.Commit()
	if err != nil {
		pqErr := err.(*pq.Error)
		log.Println(pqErr.Code)
		log.Println(err)
		return convertPqErr(pqErr.Code.Name(), pqErr.Error())
	}
	return nil
}

func (tx *Tx) CreateUser(usr *User) error {
	//time.Local = time.UTC
	_, err := tx.Exec("INSERT INTO users (username, created, phone, password, salt, email) VALUES ($1, 'now', $2, $3, $4, '')", strings.ToLower(usr.Username), usr.Phone, usr.Password, usr.Salt)
	return err
}

func (db *DB) DeleteUser(usr *User) error {
	tx, _ := db.Begin()
	err := tx.DeleteUser(usr)
	if err != nil {
		pqErr := err.(*pq.Error)
		log.Println(pqErr.Code)
		log.Println(pqErr)
		return convertPqErr(pqErr.Code.Name(), pqErr.Error())
	}
	err = tx.Commit()
	if err != nil {
		pqErr := err.(*pq.Error)
		log.Println(pqErr.Code)
		log.Println(err)
		return convertPqErr(pqErr.Code.Name(), pqErr.Error())
	}
	return nil
}

func (tx *Tx) DeleteUser(usr *User) error {
	_, err := tx.Exec("DELETE FROM users WHERE LOWER(username) = $1", strings.ToLower(usr.Username))
	return err
}

func (db *DB) GetUser(username string) (*User, error) {
	usr := new(User)
	err := db.QueryRow("SELECT username, phone FROM users WHERE LOWER(username) = $1", strings.ToLower(username)).Scan(&usr.Username, &usr.Phone) //TODO: Later remove phone number from list of items returned
	switch {
	case err == sql.ErrNoRows:
		return nil, nil
	case err != nil:
		return nil, err
	default:
		return usr, nil
	}
}

func (db *DB) LookupPass(username string) ([]byte, error) {
	var password string
	err := db.QueryRow("SELECT password FROM users WHERE LOWER(username) = $1", strings.ToLower(username)).Scan(&password)
	switch {
	case err == sql.ErrNoRows:
		return nil, nil
	case err != nil:
		return nil, err
	default:
		return []byte(password), nil
	}
}
