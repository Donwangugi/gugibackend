package main

import (
	"bitbucket.org/gugibackend/constants"
	"bitbucket.org/gugibackend/models"
	"bitbucket.org/gugibackend/pwdHash"
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

const User_Struct string = "usrStrct" //TODO: Consider moving all middlewares to a package

func HashPassword(c *gin.Context) {
	var usr models.User
	err := c.BindJSON(&usr)
	if err == nil {
		salt, err := pwdHash.GenerateSalt()
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, err)
		}
		passBytes := []byte(usr.Password)
		password, err := pwdHash.HashPassword(append(salt, passBytes...))
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, err)
		}
		usr.Password = string(password[:])
		usr.Salt = string(salt[:])
		c.Set(User_Struct, &usr) //Will this cause an issue since usr may go out of scope?
		c.Next()
	} else {
		c.AbortWithError(http.StatusBadRequest, err)
	}
}

func SetToken(c *gin.Context) {
	result, usrSaved := c.Get(User_Struct)
	resultUser := result.(models.User)

	if !usrSaved {
		c.AbortWithError(http.StatusBadRequest, errors.New("Unable to retrieve user from context."))
		return
	}

	token := jwt.New(jwt.SigningMethodHS256)
	token.Header["username"] = resultUser.Username
	token.Claims["exp"] = time.Now().Add(constants.AvgYear).Unix()
	tokenString, err := token.SignedString([]byte(resultUser.Password))
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	c.JSON(http.StatusCreated, jsonSuccess_UserPut{Text: models.Success, Token: tokenString, Logged: true})
}

func (env *Env) keyLookup(username string) ([]byte, error) {
	pass, err := env.db.LookupPass(username)
	return []byte(pass), err
}

func (env *Env) Auth(c *gin.Context) {
	tokenString := c.Request.Header.Get("authorization")

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		username := token.Header["username"].(string)
		return env.keyLookup(username)
	})

	if err != nil || !token.Valid {
		c.JSON(http.StatusUnauthorized, jsonErr{Text: err.Error(), Logged: false})
		c.Abort()
	}
}
