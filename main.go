package main

import (
	"bitbucket.org/gugibackend/models"
	"github.com/gin-gonic/gin"
	"log"
	"os"
)

type Env struct {
	db models.Datastore
} //TODO: Figure out cleaner way to add transactions to Env

func main() {

	port := os.Getenv("PORT")
	if port == "" {
		log.Fatal("$PORT must be set")
	}

	db, err := models.Open(os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Panic(err)
	}
	env := &Env{db}

	router := gin.Default()

	router.GET("/:name", env.UserGet)
	router.PUT("/:name", HashPassword, env.UserPut, SetToken)

	router.Run(":" + port)
}
