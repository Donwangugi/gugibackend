package main

import (
	"bitbucket.org/gugibackend/constants"
	"bitbucket.org/gugibackend/models"
	"bitbucket.org/gugibackend/pwdHash"
	"bytes"
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func Test_Middleware_HashPassword_Success(t *testing.T) {
	gin.SetMode(gin.TestMode)
	router := gin.New()
	router.Use(HashPassword)

	rec := httptest.NewRecorder()
	user := "test_user"
	usr := models.User{Username: user, Phone: "1234567890", Password: "password"}

	passBytes := []byte(usr.Password)

	router.GET("/:name", func(c *gin.Context) {
		result, exists := c.Get(User_Struct)
		resultUser := result.(*models.User)
		if !exists {
			t.Error("No userstruct set")
		}

		passHashBytes := []byte(resultUser.Password)
		saltBytes := []byte(resultUser.Salt)
		err := pwdHash.CompareHashAndPass(passHashBytes, append(saltBytes, passBytes...))
		if err != nil {
			t.Errorf("Error, want %q, err message: %q", passHashBytes, err.Error())
		}
	})

	btes, _ := json.Marshal(usr) //TODO: Do I need this line?
	body := bytes.NewReader(btes)

	req, _ := http.NewRequest("GET", "/"+user, body)
	router.ServeHTTP(rec, req)
}

func Test_Middleware_SetToken_Success(t *testing.T) {
	usrName := "user_test"
	pass := "password"
	usr := models.User{Username: usrName, Password: pass}
	expectedStatusCode := http.StatusCreated

	gin.SetMode(gin.TestMode)
	router := gin.New()
	router.Use(SetupUser(usr))
	router.Use(SetToken)

	rec := httptest.NewRecorder()

	//btes, _ := json.Marshal(usr) //TODO: Do I need this line?
	//body := bytes.NewReader(btes)

	req, _ := http.NewRequest("GET", "", nil)
	router.ServeHTTP(rec, req)

	if rec.Code != expectedStatusCode {
		t.Errorf("UserPut(%q) => Code:%d Code:%d ", usrName, rec.Code, http.StatusOK)
	}

	var m jsonSuccess_UserPut
	err := json.Unmarshal(rec.Body.Bytes(), &m)
	if err != nil {
		t.Errorf("Improper jsonSuccess format => %q", err)
	}

	if m.Token == "" {
		t.Error("SetToken => no token set")
	}
}

type mockDB_PasswordExists struct{}

func (mdb *mockDB_PasswordExists) GetUser(username string) (*models.User, error) {
	return nil, nil
}

func (mdb *mockDB_PasswordExists) CreateUser(*models.User) error {
	return nil
}

func (mdb *mockDB_PasswordExists) DeleteUser(*models.User) error {
	return nil
}

func (mdb *mockDB_PasswordExists) LookupPass(string) ([]byte, error) {
	return []byte("password"), nil
}

func Test_Middleware_Auth_Success(t *testing.T) {
	expectedStatusCode := http.StatusOK

	token := jwt.New(jwt.SigningMethodHS256)
	token.Header["username"] = "username"
	token.Claims["exp"] = time.Now().Add(constants.AvgYear).Unix()
	tokenString, err := token.SignedString([]byte("password"))
	if err != nil {
		t.Errorf("Unable to build token: %q", err.Error())
		return
	}

	env := Env{db: &mockDB_PasswordExists{}}
	gin.SetMode(gin.TestMode)
	router := gin.New()
	router.Use(env.Auth)

	router.GET("/index", func(c *gin.Context) {
		c.String(expectedStatusCode, "Success")
	})

	rec := httptest.NewRecorder()

	req, _ := http.NewRequest("GET", "/index", nil)
	req.Header.Add("authorization", tokenString)
	router.ServeHTTP(rec, req)

	if rec.Code != expectedStatusCode {
		var m jsonErr
		err = json.Unmarshal(rec.Body.Bytes(), &m)
		if err != nil {
			t.Errorf("Auth(password) => Code:%d Code:%d, unable to marshal error code with error:%q", rec.Code, expectedStatusCode, err.Error())
		} else {
			t.Errorf("Auth(password) => Code:%d Code:%d with error:%q", rec.Code, expectedStatusCode, m.Text)
		}
	}
}
