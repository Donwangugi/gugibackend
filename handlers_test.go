package main

import (
	"bitbucket.org/gugibackend/models"
	"bytes"
	"encoding/json"
	"errors"
	"github.com/gin-gonic/gin"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

type mockDB_NoUser struct{}

func (mdb *mockDB_NoUser) GetUser(username string) (*models.User, error) {
	return nil, nil
}

func (mdb *mockDB_NoUser) CreateUser(*models.User) error {
	return nil
}

func (mdb *mockDB_NoUser) DeleteUser(*models.User) error {
	return nil
}

func (mdb *mockDB_NoUser) LookupPass(string) ([]byte, error) {
	return nil, nil
}

func Test_Handlers_UserGet_NoUser(t *testing.T) {
	env := Env{db: &mockDB_NoUser{}}
	gin.SetMode(gin.TestMode)
	router := gin.New()
	router.GET("/:name", env.UserGet)

	rec := httptest.NewRecorder()
	user := "/test"
	req, _ := http.NewRequest("GET", user, nil)
	router.ServeHTTP(rec, req)

	expectedBody := `{"Code":404,"Text":"Not Found"}`
	if rec.Code != http.StatusNotFound && rec.Body.String() != expectedBody {
		t.Errorf("UserGet(%q) => Code:%d Body:%q, want Code:%d Body:%q", user, rec.Code, rec.Body.String(), http.StatusNotFound, expectedBody)
	}
}

type mockDB_UserExists struct{}

func (mdb *mockDB_UserExists) GetUser(username string) (*models.User, error) {
	return &models.User{Id: 1, Username: "gugi"}, nil
}

func (mdb *mockDB_UserExists) CreateUser(*models.User) error {
	return nil
}

func (mdb *mockDB_UserExists) DeleteUser(*models.User) error {
	return nil
}

func (mdb *mockDB_UserExists) LookupPass(string) ([]byte, error) {
	return nil, nil
}

func Test_Handlers_UserGet_UserExists(t *testing.T) {
	env := Env{db: &mockDB_UserExists{}}
	gin.SetMode(gin.TestMode)
	router := gin.New()
	router.GET("/:name", env.UserGet)

	rec := httptest.NewRecorder()
	user := "/gugi"
	req, _ := http.NewRequest("GET", user, nil)
	router.ServeHTTP(rec, req)

	if rec.Code != http.StatusOK {
		t.Errorf("UserGet(%q) => Code:%d Code:%d ", user, rec.Code, http.StatusOK)
	}
}

func Test_Handlers_UserGet_UserExists_CaseSensitive(t *testing.T) {
	env := Env{db: &mockDB_UserExists{}}
	gin.SetMode(gin.TestMode)
	router := gin.New()
	router.GET("/:name", env.UserGet)

	rec := httptest.NewRecorder()
	user := "/GUGI"
	req, _ := http.NewRequest("GET", user, nil)
	router.ServeHTTP(rec, req)

	if rec.Code != http.StatusOK {
		t.Errorf("UserGet(%q) => Code:%d Code:%d ", user, rec.Code, http.StatusOK)
	}
}

type mockDB_UserTaken struct{}

func (mdb *mockDB_UserTaken) GetUser(username string) (*models.User, error) {
	return nil, nil
}

func (mdb *mockDB_UserTaken) CreateUser(*models.User) error {
	return errors.New(models.Username_taken)
}

func (mdb *mockDB_UserTaken) DeleteUser(*models.User) error {
	return nil
}

func (mdb *mockDB_UserTaken) LookupPass(string) ([]byte, error) {
	return nil, nil
}

func Test_Handlers_UserPut_UserTaken(t *testing.T) {
	env := Env{db: &mockDB_UserTaken{}}
	usrName := "user_test"
	usr := models.User{Username: usrName, Created: time.Now(), Phone: "1234567890", Password: "blargh"}
	expectedStatusCode := http.StatusOK

	gin.SetMode(gin.TestMode)
	router := gin.New()
	router.Use(SetupUser(usr))
	router.PUT("/:name", env.UserPut)

	rec := httptest.NewRecorder()

	btes, _ := json.Marshal(usr)
	body := bytes.NewReader(btes)

	req, _ := http.NewRequest("PUT", "/"+usrName, body)
	router.ServeHTTP(rec, req)

	if rec.Code != expectedStatusCode {
		t.Errorf("UserPut(%q) => Code:%d Code:%d ", usrName, rec.Code, http.StatusOK)
	}

	var m jsonErr
	err := json.Unmarshal(rec.Body.Bytes(), &m)
	if err != nil {
		t.Errorf("Improper jsonErr format, err:", err)
	}

	result := jsonErr{Text: models.Username_taken, Logged: false}
	if m != result {
		t.Errorf("UserPut => {`Text`: %q, `Logged`: %t}, want {`Text`: %q, `Logged`: %t}", m.Text, m.Logged, result.Text, result.Logged)
	}
}

type mockDB_PhoneTaken struct{}

func (mdb *mockDB_PhoneTaken) GetUser(username string) (*models.User, error) {
	return nil, nil
}

func (mdb *mockDB_PhoneTaken) CreateUser(*models.User) error {
	return errors.New(models.Phone_number_in_use)
}

func (mdb *mockDB_PhoneTaken) DeleteUser(*models.User) error {
	return nil
}

func (mdb *mockDB_PhoneTaken) LookupPass(string) ([]byte, error) {
	return nil, nil
}

func Test_Handlers_UserPut_PhoneTaken(t *testing.T) {
	env := Env{db: &mockDB_PhoneTaken{}}
	usrName := "user_test"
	usr := models.User{Id: 0, Username: usrName, Created: time.Now(), Phone: "1234567890", Password: "blargh"}
	expectedStatusCode := http.StatusOK

	gin.SetMode(gin.TestMode)
	router := gin.New()
	router.Use(SetupUser(usr))
	router.PUT("/:name", env.UserPut)

	rec := httptest.NewRecorder()

	btes, _ := json.Marshal(usr)
	body := bytes.NewReader(btes)

	req, _ := http.NewRequest("PUT", "/"+usrName, body)
	router.ServeHTTP(rec, req)

	if rec.Code != expectedStatusCode {
		t.Errorf("UserPut(%q) => Code:%d Code:%d ", usrName, rec.Code, http.StatusOK)
	}

	var m jsonErr
	err := json.Unmarshal(rec.Body.Bytes(), &m)
	if err != nil {
		t.Errorf("Improper jsonErr format => %q", err)
	}

	result := jsonErr{Text: models.Phone_number_in_use, Logged: false}
	if m != result {
		t.Errorf("UserPut => {`Text`: %q, `Logged`: %t}, want {`Text`: %q, `Logged`: %t}", m.Text, m.Logged, result.Text, result.Logged)
	}
}

type mockDB_UserPutSuccess struct{}

func (mdb *mockDB_UserPutSuccess) GetUser(username string) (*models.User, error) {
	return nil, nil
}

func (mdb *mockDB_UserPutSuccess) CreateUser(*models.User) error {
	return nil
}

func (mdb *mockDB_UserPutSuccess) DeleteUser(*models.User) error {
	return nil
}

func (mdb *mockDB_UserPutSuccess) LookupPass(string) ([]byte, error) {
	return nil, nil
}

func SetupUser(usr models.User) gin.HandlerFunc {
	return func(c *gin.Context) {
		//c.Params = append(c.Params, gin.Param{Key: "name", Value: usr.Username})
		c.Set(User_Struct, usr)
	}
}

func Test_Handlers_UserPut_Success(t *testing.T) {
	env := Env{db: &mockDB_UserPutSuccess{}}
	usrName := "user_test"
	usr := models.User{Id: 0, Username: usrName, Created: time.Now(), Phone: "1234567890", Password: "blargh"}
	expectedStatusCode := http.StatusOK

	gin.SetMode(gin.TestMode)
	router := gin.New()
	router.Use(SetupUser(usr))
	router.PUT("/:name", env.UserPut)

	rec := httptest.NewRecorder()

	btes, _ := json.Marshal(usr)
	body := bytes.NewReader(btes)

	req, _ := http.NewRequest("PUT", "/"+usrName, body)
	router.ServeHTTP(rec, req)

	if rec.Code != expectedStatusCode {
		t.Errorf("UserPut(%q) => Code:%d want Code:%d ", usrName, rec.Code, expectedStatusCode)
	}

	if rec.Body.String() != "" {
		t.Errorf("Userput, unexpected response body: %q", rec.Body.String())
	}
}

func Test_Handlers_UserPut_Success_MissingUserFields(t *testing.T) {
	env := Env{db: &mockDB_UserPutSuccess{}}
	usrName := "user_test"
	usr := models.User{Username: usrName, Phone: "1234567890", Password: "blargh"}
	expectedStatusCode := http.StatusOK

	gin.SetMode(gin.TestMode)
	router := gin.New()
	router.Use(SetupUser(usr))
	router.PUT("/:name", env.UserPut)

	rec := httptest.NewRecorder()

	btes, _ := json.Marshal(usr)
	body := bytes.NewReader(btes)

	req, _ := http.NewRequest("PUT", "/"+usrName, body)
	router.ServeHTTP(rec, req)

	if rec.Code != expectedStatusCode {
		t.Errorf("UserPut(%q) => Code:%d Code:%d ", usrName, rec.Code, expectedStatusCode)
	}

	if rec.Body.String() != "" {
		t.Errorf("Userput, unexpected response body: %q", rec.Body.String())
	}

}

func Test_Handlers_UserPut_Fail_UsernameMismatch(t *testing.T) {
	env := Env{db: &mockDB_UserPutSuccess{}}
	usrName := "not_user_test"
	usr := models.User{Username: usrName, Phone: "1234567890", Password: "blargh"}

	gin.SetMode(gin.TestMode)
	router := gin.New()
	router.Use(SetupUser(usr))
	router.PUT("/:name", env.UserPut)

	rec := httptest.NewRecorder()

	btes, _ := json.Marshal(usr)
	body := bytes.NewReader(btes)

	req, _ := http.NewRequest("PUT", "/"+"not_usr_test", body)
	router.ServeHTTP(rec, req)

	recCode := http.StatusBadRequest
	if rec.Code != recCode {
		t.Errorf("UserPut(%q) => Code:%d Code:%d ", "not_usr_test", rec.Code, recCode)
	}

	var m jsonErr
	err := json.Unmarshal(rec.Body.Bytes(), &m)
	if err != nil {
		t.Errorf("Improper jsonErr format => %q", err)
	}

	result := jsonErr{Text: models.Mismatching_Username_Data, Logged: false}
	if m != result {
		t.Errorf("UserPut => {`Text`: %q, `Logged`: %t}, want {`Text`: %q, `Logged`: %t}", m.Text, m.Logged, result.Text, result.Logged)
	}
}

type mockDB_UnkownError struct{}

func (mdb *mockDB_UnkownError) GetUser(username string) (*models.User, error) {
	return nil, nil
}

func (mdb *mockDB_UnkownError) CreateUser(*models.User) error {
	return errors.New(models.Unknown_Error)
}

func (mdb *mockDB_UnkownError) DeleteUser(*models.User) error {
	return nil
}

func (mdb *mockDB_UnkownError) LookupPass(string) ([]byte, error) {
	return nil, nil
}

func Test_Handlers_UserPut_UnkownError(t *testing.T) {
	env := Env{db: &mockDB_UnkownError{}}
	usrName := "user_test"
	usr := models.User{Id: 0, Username: usrName, Created: time.Now(), Phone: "1234567890", Password: "blargh"}

	gin.SetMode(gin.TestMode)
	router := gin.New()
	router.Use(SetupUser(usr))
	router.PUT("/:name", env.UserPut)

	rec := httptest.NewRecorder()

	btes, _ := json.Marshal(usr)
	body := bytes.NewReader(btes)

	req, _ := http.NewRequest("PUT", "/"+usrName, body)

	router.ServeHTTP(rec, req)

	if rec.Code != http.StatusOK {
		t.Errorf("UserPut(%q) => Code:%d Code:%d ", usrName, rec.Code, http.StatusOK)
	}

	var m jsonErr
	err := json.Unmarshal(rec.Body.Bytes(), &m)
	if err != nil {
		t.Errorf("Improper jsonErr format => %q", err)
	}

	result := jsonErr{Text: models.Unknown_Error, Logged: false}
	if m != result {
		t.Errorf("UserPut => {`Text`: %q, `Logged`: %t}, want {`Text`: %q, `Logged`: %t}", m.Text, m.Logged, result.Text, result.Logged)
	}
}
